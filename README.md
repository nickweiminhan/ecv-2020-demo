## 介绍
ECV 2020 四个赛道的示例代码

## 使用说明
四个文件加分别对应四个赛道的示例代码，包括训练代码`xxx-detect/train`和OpenVINO模型的封装代码`xxx-detect/ev_sdk`，示例代码均可以直接拷贝使用。

1.  老鼠识别：`rat-detect`
2.  火焰识别：`fire-detect`
3.  摔倒识别：`fall-detect`
4.  电动车头盔识别识别：`motor-head-detect`

示例代码的使用，请参考[视频教程](https://outin-58837e35b78011e9b1eb00163e1a65b6.oss-cn-shanghai.aliyuncs.com/sv/542a789b-173a937f5d7/542a789b-173a937f5d7.mp4)。